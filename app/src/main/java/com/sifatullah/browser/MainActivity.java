package com.sifatullah.browser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private Button button_search;
    private EditText url_text;
    private WebView browser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openUrl();
    }

//    public void openUrl() {
//        button_search =(Button)findViewById(R.id.button);
//        url_text =(EditText)findViewById(R.id.editText);
//        browser = (WebView)findViewById(R.id.webView);
//
//        button_search.setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        String url = url_text.getText().toString();
//                        browser.getSettings().setLoadsImagesAutomatically(true);
//                        browser.getSettings().setJavaScriptEnabled(true);
//                        browser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//                        browser.setWebViewClient(new WebViewClient());
//                        browser.loadUrl(url);
//                    }
//                }
//        );
//    }

    public void openUrl(){

        button_search= findViewById(R.id.button);
        url_text= findViewById(R.id.editText);
        browser= findViewById(R.id.webView);

        button_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    String url= url_text.getText().toString();
                    browser.getSettings().setLoadsImagesAutomatically(true);
                    browser.getSettings().setJavaScriptEnabled(true);
                    browser.setWebViewClient(new WebViewClient());
                    browser.loadUrl(url);
                }

                catch (Exception e){
                    e.printStackTrace();
                }


            }
        });

    }


}
