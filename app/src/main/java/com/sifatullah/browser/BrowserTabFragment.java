package com.sifatullah.browser;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Sifat Ullah on 5/17/2018.
 */

public class BrowserTabFragment extends Fragment {

    private  Button button_search;
    private  EditText url_text;
    private  WebView browser;

    private static final String ARG_SECTION_NUMBER = "section_number";

    public static BrowserTabFragment newInstance(int sectionNumber) {
        BrowserTabFragment fragment = new BrowserTabFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =

         inflater.inflate(R.layout.activity_main, container, false);

        button_search= rootView.findViewById(R.id.button);
        url_text= rootView.findViewById(R.id.editText);
        browser= rootView.findViewById(R.id.webView);

        button_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    String url= url_text.getText().toString();
                    browser.getSettings().setLoadsImagesAutomatically(true);
                    browser.getSettings().setJavaScriptEnabled(true);
                    browser.setWebViewClient(new WebViewClient());
                    browser.loadUrl(url);
                }

                catch (Exception e){
                    e.printStackTrace();
                }


            }
        });
        return rootView;
    }


}